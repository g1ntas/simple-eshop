Simple e-shop
=============

### Install

1. `composer install`
2. Set your database login data in `app/config/parameters.yml`
3. `php bin/console doctrine:database:create`
4. `php bin/console doctrine:schema:update --force`
5. `php bin/console server:run`
6. Enjoy