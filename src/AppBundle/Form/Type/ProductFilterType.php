<?php

namespace AppBundle\Form\Type;

use AppBundle\Model\ProductFilter;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductFilterType
 *
 * @package AppBundle\Form\Type
 */
class ProductFilterType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->setMethod('GET');
        $builder
            ->add('category', EntityType::class, [
                'class' => 'AppBundle:Category',
                'choice_label' => 'name',
                'label' => 'Category',
                'required' => false,
            ])
            ->add('sort', ChoiceType::class, [
                'label' => 'Sort by',
                'choices' => [
                    '' => null,
                    'Latest products' => ProductFilter::SORT_LATEST_PRODUCTS,
                    'Highest price' => ProductFilter::SORT_HIGHEST_PRICE,
                    'Lowest price' => ProductFilter::SORT_LOWEST_PRICE,
                ]
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductFilter::class,
            'csrf_protection'   => false,
            'required' => false,
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'app_product_filter';
    }
}
