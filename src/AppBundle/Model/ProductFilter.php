<?php

namespace AppBundle\Model;

/**
 * Class ProductFilter
 *
 * @package AppBundle\Model
 */
class ProductFilter
{
    const SORT_LATEST_PRODUCTS = 'latest-products';
    const SORT_LOWEST_PRICE = 'lowest-price';
    const SORT_HIGHEST_PRICE = 'highest-price';

    /**
     * @var \AppBundle\Entity\Category
     */
    private $category;

    /**
     * @var string
     */
    private $sort;

    /**
     * @return \AppBundle\Entity\Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param \AppBundle\Entity\Category $category
     *
     * @return $this
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return string
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param string $sort
     *
     * @return $this
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }
}