<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CartController
 *
 * @package AppBundle\Controller
 *
 * @Route("/cart")
 */
class CartController extends Controller
{
    /**
     * @Route("", name="cart_index")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Template("cart/index.html.twig")
     *
     * @return array
     */
    public function indexAction()
    {
        $cartService = $this->get('app.service.cart');

        return [
            'cart' => $cartService->all(),
            'totalPrice' => $cartService->getTotalPrice()
        ];
    }

    /**
     * @Template("cart/widget.html.twig")
     *
     * @return array
     */
    public function widgetAction()
    {
        return [
            'cart' => $this->get('app.service.cart')->all(),
        ];
    }

    /**
     * @Route("/add-product", name="cart_add_product")
     * @Method({"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addProductAction(Request $request)
    {
        $productId = $request->request->get('id');
        $quantity = (int) $request->request->get('quantity', 1);

        if (! is_null($productId)) {
            $this->get('app.service.cart')->add($productId, $quantity);
        }

        return new Response();
    }

    /**
     * @Route("/remove-product", name="cart_remove_product")
     * @Method({"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function removeProductAction(Request $request)
    {
        $productId = $request->request->get('id');

        if (! is_null($productId)) {
            $this->get('app.service.cart')->remove($productId);
        }

        return new Response();
    }
}
