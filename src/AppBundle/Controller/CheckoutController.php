<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;

/**
 * Class CheckoutController
 *
 * @package AppBundle\Controller
 */
class CheckoutController extends Controller
{
    /**
     * @Route("/checkout", name="checkout_index")
     * @Method("POST")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexAction()
    {
        $cartService = $this->get('app.service.cart');

        $cartProducts = $cartService->all();

        if (empty($cartProducts)) {
            throw new Exception('Cart is empty');
        }

        $this->get('app.service.checkout')->createRequests($this->getUser(), $cartProducts);

        $cartService->clean();

        $this->addFlash('success', $this->get('translator.default')->trans('Requests was successfully created'));

        return $this->redirectToRoute('homepage');
    }
}
