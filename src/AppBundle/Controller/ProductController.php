<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Form\Type\ProductFilterType;
use AppBundle\Form\Type\ProductType;
use AppBundle\Model\ProductFilter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProductController
 *
 * @package AppBundle\Controller
 *
 * @Route("/products")
 */
class ProductController extends Controller
{
    /**
     * @Route("", name="product_index")
     * @Template("product/index.html.twig")
     *
     * @return array
     */
    public function indexAction(Request $request)
    {
        $productFilter = new ProductFilter();

        $form = $this->createForm(ProductFilterType::class, $productFilter);
        $form->handleRequest($request);

        $query = $this->getDoctrine()->getRepository('AppBundle:Product')->getAllProductsQuery($productFilter);

        $pagination = $this->get('knp_paginator')->paginate(
            $query,
            $request->query->getInt('page', 1),
            $this->getParameter('product.pagination_limit')
        );

        return [
            'filterForm' => $form->createView(),
            'pagination' => $pagination,
        ];
    }

    /**
     * @Route("/create", name="product_create")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * @Template("product/create.html.twig")
     *
     * @param Request $request
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request)
    {
        $product = new Product();

        $product->setUser($this->getUser());

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($product);
            $em->flush();

            $this->addFlash('success', $this->get('translator.default')->trans('Product was successfully created'));

            return $this->redirectToRoute('product_index');
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/show/{product}", name="product_show")
     * @Template("product/show.html.twig")
     *
     * @param Product $product
     *
     * @return array
     */
    public function showAction(Product $product)
    {
        return [
            'product' => $product,
        ];
    }
}
