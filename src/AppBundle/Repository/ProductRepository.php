<?php

namespace AppBundle\Repository;

use AppBundle\Model\ProductFilter;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class ProductRepository extends EntityRepository
{
    /**
     * @param ProductFilter $productFilter
     *
     * @return array
     */
    public function getAllProductsQuery(ProductFilter $productFilter)
    {
        $builder = $this->createQueryBuilder('p');

        $this->applyFilters($productFilter, $builder);

        return $builder->getQuery();
    }

    /**
     * @param ProductFilter $productFilter
     * @param QueryBuilder $builder
     */
    private function applyFilters(ProductFilter $productFilter, QueryBuilder $builder)
    {
        if (! is_null($productFilter->getCategory())) {
            $builder
                ->andWhere('p.category = :category')
                ->setParameter('category', $productFilter->getCategory());
        }

        switch ($productFilter->getSort()) {
            case ProductFilter::SORT_LATEST_PRODUCTS:
                $builder->orderBy('p.updatedAt', 'DESC');
                break;
            case ProductFilter::SORT_HIGHEST_PRICE:
                $builder->orderBy('p.price', 'DESC');
                break;
            case ProductFilter::SORT_LOWEST_PRICE:
                $builder->orderBy('p.price', 'ASC');
                break;
        }
    }
}