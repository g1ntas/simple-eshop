<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Class BuyRequest
 *
 * @package AppBundle\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="buy_requests")
 */
class BuyRequest
{
    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="ProductStack", cascade={"persist"})
     * @ORM\JoinTable(name="buy_requests_product_stacks",
     *      joinColumns={@ORM\JoinColumn(name="buy_request_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="product_stack_id", referencedColumnName="id")}
     * )
     */
    private $productStacks;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="buyer_id", referencedColumnName="id")
     */
    private $buyer;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="seller_id", referencedColumnName="id")
     */
    private $seller;

    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->productStacks = new ArrayCollection();
        $this->createdAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getProductStacks()
    {
        return $this->productStacks;
    }

    /**
     * @param ArrayCollection $productStacks
     *
     * @return $this
     */
    public function setProductStacks($productStacks)
    {
        $this->productStacks = $productStacks;

        return $this;
    }

    /**
     * @return User
     */
    public function getBuyer()
    {
        return $this->buyer;
    }

    /**
     * @param User $buyer
     *
     * @return $this
     */
    public function setBuyer($buyer)
    {
        $this->buyer = $buyer;

        return $this;
    }

    /**
     * @return User
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     * @param User $seller
     *
     * @return $this
     */
    public function setSeller($seller)
    {
        $this->seller = $seller;

        return $this;
    }
}