<?php

namespace AppBundle\Service;

use AppBundle\Entity\BuyRequest;
use AppBundle\Entity\User;
use AppBundle\Entity\ProductStack;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class CheckoutService
 *
 * @package AppBundle\Service
 */
class CheckoutService
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * CheckoutService constructor.
     *
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param User $buyer
     * @param array $productStacks
     */
    public function createRequests(User $buyer, array $productStacks)
    {
        $groupedItems = $this->groupProductsBySeller($productStacks);

        foreach ($groupedItems as $item) {
            $buyRequest = (new BuyRequest())
                ->setBuyer($buyer)
                ->setSeller($item['user'])
                ->setProductStacks(new ArrayCollection($item['items']))
            ;

            $this->objectManager->persist($buyRequest);
        }

        $this->objectManager->flush();
    }

    /**
     * @param array $productStacks
     *
     * @return array
     */
    private function groupProductsBySeller(array $productStacks)
    {
        $items = [];

        foreach ($productStacks as $item) {
            if ($item instanceof ProductStack) {
                $user = $item->getProduct()->getUser();

                $items[$user->getId()]['user'] = $user;
                $items[$user->getId()]['items'][] = $item;
            }
        }

        return $items;
    }
}
