<?php

namespace AppBundle\Service;

use AppBundle\Entity\Product;
use AppBundle\Entity\ProductStack;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class CartService
 *
 * @package AppBundle\Service
 */
class CartService
{
    const SESSION_KEY = '_cart_products';

    /**
     * @var SessionInterface
     */
    protected $session;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * CartService constructor.
     *
     * @param ObjectManager $objectManager
     * @param SessionInterface $session
     */
    public function __construct(ObjectManager $objectManager, SessionInterface $session)
    {
        $this->objectManager = $objectManager;
        $this->session = $session;
    }

    /**
     * @param integer $productId
     * @param integer $quantity
     */
    public function add($productId, $quantity)
    {
        $cart = $this->session->get(self::SESSION_KEY, []);

        $productRepository = $this->getRepository();
        $product = $productRepository->find($productId);

        if (! $product instanceof Product) {
            throw new NotFoundHttpException('Product not found');
        }

        $cart[$product->getId()] = ['quantity' => $quantity];

        $this->session->set(self::SESSION_KEY, $cart);
    }

    /**
     * @return array
     */
    public function all()
    {
        $cart = $this->session->get(self::SESSION_KEY, []);

        if (empty($cart)) {
            return [];
        }

        $products = $this->getRepository()->findBy(['id' => array_keys($cart)]);

        /** @var \AppBundle\Entity\Product $product */
        foreach ($products as $product) {
            $cartProduct = (new ProductStack($product))
                ->setQuantity($cart[$product->getId()]['quantity'])
            ;

            $cart[$product->getId()] = $cartProduct;
        }

        return array_reverse($cart);
    }

    /**
     * @param integer $productId
     */
    public function remove($productId)
    {
        $cart = $this->session->get(self::SESSION_KEY, []);

        if (isset($cart[$productId])) {
            unset($cart[$productId]);

            $this->session->set(self::SESSION_KEY, $cart);
        }
    }

    /**
     * @return integer
     */
    public function getTotalPrice()
    {
        $price = array_reduce($this->all(), function($total, ProductStack $item) {
            return $total + $item->getTotalPrice();
        });

        return $price;
    }

    public function clean()
    {
        $this->session->set(self::SESSION_KEY, []);
    }

    /**
     * @return \AppBundle\Repository\ProductRepository
     */
    private function getRepository()
    {
        $productRepository = $this->objectManager->getRepository('AppBundle:Product');

        return $productRepository;
    }
}