<?php

namespace AppBundle\Service;

use AppBundle\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

/**
 * Class UserPasswordService
 *
 * @package AppBundle\Service
 */
class UserPasswordService
{
    /**
     * @var UserPasswordEncoder
     */
    private $userPasswordEncoder;

    /**
     * UserPasswordService constructor.
     *
     * @param UserPasswordEncoder $userPasswordEncoder
     */
    public function __construct(UserPasswordEncoder $userPasswordEncoder)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
    }

    /**
     * @param User $user
     */
    public function setEncodedPassword(User $user)
    {
        if (! is_null($user->getPlainPassword())) {
            $password = $this->userPasswordEncoder->encodePassword($user, $user->getPlainPassword());

            $user->setPassword($password);
            $user->eraseCredentials();
        }
    }
}