<?php

namespace Tests\AppBundle\Service;

use AppBundle\Entity\User;
use AppBundle\Service\UserPasswordService;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;

/**
 * Class UserPasswordServiceTest
 *
 * @package Tests\AppBundle\Service
 */
class UserPasswordServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var UserPasswordService
     */
    private $userPasswordService;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $userPasswordEncoder;

    public function setUp()
    {
        $this->userPasswordEncoder = $this->getMockBuilder(UserPasswordEncoder::class)->disableOriginalConstructor()->getMock();

        $this->userPasswordService = new UserPasswordService($this->userPasswordEncoder);
    }

    /**
     * @test
     */
    public function it_should_encode_and_set_user_password()
    {
        $user = (new User())->setPlainPassword('test');

        $this->assertNull($user->getPassword());

        $this->userPasswordEncoder
            ->expects($this->once())
            ->method('encodePassword')
            ->with($user, 'test')
            ->willReturn('encoded_password')
        ;

        $this->userPasswordService->setEncodedPassword($user);

        $this->assertEquals('encoded_password', $user->getPassword());
    }

    /**
     * @test
     */
    public function it_should_do_nothing_if_password_is_empty()
    {
        $this->userPasswordEncoder->expects($this->never())->method('encodePassword');

        $this->userPasswordService->setEncodedPassword(new User());
    }
}
