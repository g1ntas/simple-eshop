<?php

namespace Tests\AppBundle\Service;

use AppBundle\Entity\Product;
use AppBundle\Entity\ProductStack;
use AppBundle\Entity\User;
use AppBundle\Service\CheckoutService;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class CheckoutServiceTest
 *
 * @package Tests\AppBundle\Service
 */
class CheckoutServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var CheckoutService
     */
    private $checkoutService;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $objectManager;

    public function setUp()
    {
        $this->objectManager = $this->getMockBuilder(ObjectManager::class)->disableOriginalConstructor()->getMock();

        $this->checkoutService = new CheckoutService($this->objectManager);
    }

    /**
     * @test
     */
    public function it_should_group_product_stacks_by_sellers_and_create_requests_for_each()
    {
        $user = $this->getMock(User::class);
        $product = (new Product())->setUser($user);
        $productStack = new ProductStack($product);

        $user->expects($this->any())->method('getId')->willReturn(1);
        $this->objectManager->expects($this->once())->method('persist');
        $this->objectManager->expects($this->once())->method('flush');

        $this->checkoutService->createRequests(new User(), [$productStack]);
    }
}
