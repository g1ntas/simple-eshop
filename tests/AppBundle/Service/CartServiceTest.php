<?php

namespace Tests\AppBundle\Service;

use AppBundle\Entity\Product;
use AppBundle\Repository\ProductRepository;
use AppBundle\Service\CartService;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class CartServiceTest
 *
 * @package Tests\AppBundle\Service
 */
class CartServiceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $objectManager;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $session;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject
     */
    private $repository;

    public function setUp()
    {
        $this->objectManager = $this->getMockBuilder(ObjectManager::class)->disableOriginalConstructor()->getMock();
        $this->session = $this->getMockBuilder(Session::class)->disableOriginalConstructor()->getMock();
        $this->repository = $this->getMockBuilder(ProductRepository::class)->disableOriginalConstructor()->getMock();

        $this->cartService = new CartService($this->objectManager, $this->session);
    }

    /**
     * @test
     */
    public function it_should_add_item_to_session()
    {
        $product = $this->getMock(Product::class);

        $this->session->expects($this->once())->method('get')->willReturn([5 => ['quantity' => 1]]);
        $this->objectManager->expects($this->once())->method('getRepository')->willReturn($this->repository);
        $this->repository->expects($this->once())->method('find')->with(1)->willReturn($product);
        $product->expects($this->once())->method('getId')->willReturn(1);
        $this->session->expects($this->once())->method('set')->with(CartService::SESSION_KEY, [
            1 => ['quantity' => 5],
            5 => ['quantity' => 1],
        ]);

        $this->cartService->add(1, 5);
    }

    /**
     * @test
     * @expectedException \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @expectedExceptionMessage Product not found
     */
    public function it_should_throw_exception_if_product_does_not_exist()
    {
        $this->objectManager->expects($this->once())->method('getRepository')->willReturn($this->repository);

        $this->cartService->add(1, 1);
    }

    /**
     * @test
     */
    public function it_should_return_all_cart_products_from_session()
    {
        $product = $this->getMock(Product::class);

        $this->session->expects($this->once())->method('get')->willReturn([
            1 => ['quantity' => 10],
        ]);

        $product->expects($this->any())->method('getId')->willReturn(1);
        $this->objectManager->expects($this->once())->method('getRepository')->willReturn($this->repository);
        $this->repository->expects($this->once())->method('findBy')->with(['id' => [1]])->willReturn([$product]);

        $productStacks = $this->cartService->all();

        $this->assertEquals(10, $productStacks[0]->getQuantity());
        $this->assertEquals($product, $productStacks[0]->getProduct());
    }

    /**
     * @test
     */
    public function it_should_return_empty_array_if_there_is_no_items_in_session()
    {
        $productStacks = $this->cartService->all();

        $this->assertEmpty($productStacks);
    }

    /**
     * @test
     */
    public function it_should_remove_item_from_session()
    {
        $this->session->expects($this->once())->method('get')->willReturn([
            1 => ['quantity' => 10],
        ]);

        $this->session->expects($this->once())->method('set')->with(CartService::SESSION_KEY, []);

        $this->cartService->remove(1);
    }

    /**
     * @test
     */
    public function it_should_reset_session()
    {
        $this->session->expects($this->once())->method('set')->with(CartService::SESSION_KEY, []);

        $this->cartService->clean();
    }
}
