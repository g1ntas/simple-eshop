<?php
namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class DefaultControllerTest extends WebTestCase
{
    /**
     * @var \Symfony\Bundle\FrameworkBundle\Client
     */
    private $client;
    
    public function setUp()
    {
        $this->client = self::createClient();
    }

    /**
     * @test
     * @dataProvider getPublicUrls
     */
    public function pageIsSuccessful($url)
    {
        $this->client->request('GET', $url);

        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }



    /**
     * @test
     * @dataProvider getSecuredUrls
     */
    public function securedPageIsSuccessful($url)
    {
        $this->client->request('GET', $url);

        $this->assertTrue($this->client->getResponse()->isRedirect());

        $this->assertEquals(
            'http://localhost/login',
            $this->client->getResponse()->getTargetUrl()
        );
    }

    /**
     * @return array
     */
    public function getPublicUrls()
    {
        return [
            ['/'],
            ['/login'],
            ['/register'],
            ['/products'],
        ];
    }

    /**
     * @return array
     */
    public function getSecuredUrls()
    {
        return [
            ['/change-password'],
            ['/products/create'],
            ['/cart'],
        ];
    }
}