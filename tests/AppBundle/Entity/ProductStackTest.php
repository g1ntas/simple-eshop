<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\Product;
use AppBundle\Entity\ProductStack;

/**
 * Class ProductStackTest
 *
 * @package Tests\AppBundle\Entity
 */
class ProductStackTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function it_should_set_quantity_by_default()
    {
        $productStack = new ProductStack(new Product());

        $this->assertEquals(1, $productStack->getQuantity());
    }

    /**
     * @test
     */
    public function it_should_set_get_total_price_of_all_stacked_products()
    {
        $product = (new Product())->setPrice(10.00);
        $productStack = (new ProductStack($product))->setQuantity(5);

        $this->assertEquals(50, $productStack->getTotalPrice());
    }
}
