<?php

namespace Tests\AppBundle\Entity;

use AppBundle\Entity\User;

/**
 * Class UserTest
 *
 * @package Tests\AppBundle\Entity
 */
class UserTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @test
     */
    public function it_should_set_salt_by_default()
    {
        $user = new User();

        $this->assertNotNull($user->getSalt());
    }

    /**
     * @test
     */
    public function it_should_unset_user_plain_password()
    {
        $user = new User();

        $user->setPlainPassword('test');

        $this->assertEquals('test', $user->getPlainPassword());

        $user->eraseCredentials();

        $this->assertNull($user->getPlainPassword());
    }
}
